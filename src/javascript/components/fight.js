import {controls} from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
    return new Promise((resolve) => {
        // resolve the promise with the winner when fight is over

        let healthLeft = firstFighter.health,
            healthRight = secondFighter.health;

        document.addEventListener('keyup', (e) => {
            switch (e.code) {
                case controls.PlayerOneAttack:
                    healthRight -= getDamage(firstFighter, secondFighter);
                    if(healthIndicatorWidth('right', healthRight, secondFighter.health)){
                        resolve(firstFighter);
                    }
                    break;
                case controls.PlayerTwoAttack:
                    healthLeft -= getDamage(secondFighter, firstFighter);
                    if(healthIndicatorWidth('left', healthLeft, firstFighter.health)){
                        resolve(secondFighter);
                    }
                    break;
            }
        });

        //resolve(firstFighter)
    });
}

export function getDamage(attacker, defender) {
    const damage = getHitPower(attacker) - getBlockPower(defender);
    return damage < 0 ? 0 : damage;
}

export function getHitPower({ attack }) {
    return attack * (Math.random() + 1);
}

export function getBlockPower({ defense }) {
    return defense * (Math.random() + 1);
}

function healthIndicatorWidth(position, health, fullHealth) {
    const pos = (health * 100) / fullHealth;
    document.getElementById(`${position}-fighter-indicator`).style.width = `${pos}%`;
    return pos < 0
}