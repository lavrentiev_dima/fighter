import { showModal } from './modal'
import { createFighterInfo } from '../fighterPreview'

export function showWinnerModal(fighter) {
  // call showModal function
    showModal({
        title: 'Winner',
        bodyElement: createFighterInfo(fighter),
        onClose: () => {}
    })

}
