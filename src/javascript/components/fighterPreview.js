import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if(fighter){
    fighterElement.append(createFighterInfo(fighter));
  }

  // todo: show fighter info (image, name, health, etc.)
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterInfo(fighter) {
  const { name, attack, defense, health } = fighter;
  const fighterInfo = createElement({
    tagName: 'div',
    className: `fighter-preview___info`,
  });
  fighterInfo.innerHTML = `
      <div class="fighter-preview___name">${name}</div>
      <div class="fighter-preview___health">Health : ${health}</div>
      <div class="fighter-preview___attack">Attack : ${attack}</div>
      <div class="fighter-preview___defense">Defense : ${defense}</div>
    `;
  fighterInfo.prepend(createFighterImage(fighter));
  return fighterInfo;
}
